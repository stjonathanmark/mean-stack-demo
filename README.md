## Description: 
A demo app that is created with the MEAN stack using Angular 6.

## Instructions 
1. To run this application you will need to download and install **NodeJs** and **MongoDB**. [Download NodeJs](https://nodejs.org/en/) - [Download MongoDB](https://www.mongodb.com/try/download/community) 
2. You need to create database named **'apidata'** on the MongoDB server and then create two collections in that database named **'persons'** and **'users'**
3. Insert the following two documents into the **'users'** collection in the **'apidata'** database on the MongoDB server
    * { username: "admin", password: "password", role: "Administrator" }
    * { username: "user", password: "password", role: "User"}
4. Modify the **'url'** variable to point in to the correct MongoDB server on **line 4** in the [**'data-source.js'**](https://bitbucket.org/stjonathanmark/mean-stack-demo/src/master/MeanWebApp/server/data/data-source.js) code file located in the 'server/data' folder 
5. Open the NodeJs command line tool and set it's path to the **'MeanWebApp'** folder (a. k. a. Application Root) where ever you placed it on your computer after download 
6. Type and run the command **'npm install'** in the NodeJs command line tool to install all the dependencies from the **'package.json'** file 
7. Type and run the command **'ng build'** in the NodeJs command line tool to compile the Angular files into bundles and place them in the newly created **'dist'** folder
8. Type and run the command **'node server'** in the NodeJs command line tool to start node server hosting the website 
9. Open any browser and type **'http://localhost:3000'** in the address bar and press enter 
10. Enjoy!!!!!!!