var express = require("express");
var passport = require("passport");
var bodyParser = require("body-parser");
var path = require("path");
var http = require("http");
var fs = require("fs");

var bearerStrategy = require("./server/strategies/bearer");

bearerStrategy.configure(passport);

var api = require("./server/routes/api");
var secureApi = require("./server/routes/secure-api");

fs.readdirSync(path.join(__dirname, "server/models")).forEach(function(
  fileName
) {
  require(path.join(__dirname, "server/models/" + fileName));
});

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "dist")));

app.use(function(req, res, next) {
  res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
  res.header("Expires", "-1");
  res.header("Pragma", "no-cache");
  next();
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(
  "/api/secure",
  passport.authenticate("bearer", { session: false }),
  secureApi
);

app.use("/api", api);

app.get("*", function(req, res, next) {
  var returnDefault = true;
  var fileType = [".ico", ".js", ".map", ".css", ".png", ".jpg", ".gif"];

  fileType.forEach((val, idx, arr) => {
    if (req.url.endsWith(val)) {
      returnDefault = false;
      return;
    }
  });

  if (returnDefault) {
    res.sendFile(path.join(__dirname, "dist/index.html"));
  } else {
    //res.sendFile(path.join(__dirname, "dist" + req.url));
    next();
  }
});

const port = process.env.PORT || 3000;

app.set("port", port);

const server = http.createServer(app);

server.listen(port, () => {
  console.log("App is running on port 3000");
});

//app.listen(process.env.PORT);
