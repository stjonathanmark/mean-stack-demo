var express = require("express");
var data = require("../data/data-source");
var jwt = require("jsonwebtoken");

var router = express.Router();

router.route("/security/authenticate").post(function(req, res) {
  data.getUserByUsername(req.body.username, function(err, user) {
    if (err) {
      res
        .status(500)
        .json({ message: "An unknown error occurred authenticating user." });
    } else {
      var response = {
        authenticated: false,
        token: ""
      };

      if (user) {
        if (user.password == req.body.password) {
          response.authenticated = true;

          jwt.sign(
            {
              uid: user._id,
              rle: user.role
            },
            "InTheNameofJesus",
            {
              subject: user.username,
              expiresIn: "1h",
              audience: "http://mean.stjonathanmark.com",
              issuer: "http://mean.stjonathanmark.com"
            },
            function(err, token) {
              if (err) {
                res
                  .status(500)
                  .send({ message: "An error occured creating token." });
              } else {
                response.token = token;
                res.send(response);
              }
            }
          );
        } else {
          res.send(response);
        }
      } else {
        res.send(response);
      }
    }
  });
});

module.exports = router;
