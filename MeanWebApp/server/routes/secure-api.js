var express = require("express");
var data = require("../data/data-source");

var router = express.Router();

router
  .route("/persons")
  .get(function(req, res) {
    data.getAllPersons(function(err, response) {
      if (err) {
        res
          .status(500)
          .json({ message: "An unknown error occurred getting all persons." });
      } else {
        res.json(response);
      }
    });
  })
  .post(function(req, res) {
    if (req.user.rle != "Administrator") {
      res.status(401).json({
        message: "Current user is not authorized to access requested resource."
      });
    } else {
      data.createPerson(req.body.person, function(err, response) {
        if (err) {
          res
            .status(500)
            .json({ message: "An unknown error occurred creating person." });
        } else {
          res.json(response);
        }
      });
    }
  })
  .put(function(req, res) {
    data.updatePerson(req.body.person, function(err, response) {
      if (err) {
        res
          .status(500)
          .json({ message: "An unknown error occurred updating person." });
      } else {
        res.json(response);
      }
    });
  });

router
  .route("/persons/:id")
  .get(function(req, res) {
    data.getPersonById(req.params.id, function(err, response) {
      if (err) {
        res
          .status(500)
          .json({ message: "An unknown error occurred getting person." });
      } else {
        res.json(response);
      }
    });
  })
  .delete(function(req, res) {
    if (req.user.rle != "Administrator") {
      res.status(401).json({
        message: "Current user is not authorized to access requested resource."
      });
    } else {
      data.deletePerson(req.params.id, function(err, response) {
        if (err) {
          res
            .status(500)
            .json({ message: "An unknown error occurred deleting person." });
        } else {
          res.json(response);
        }
      });
    }
  });

module.exports = router;
