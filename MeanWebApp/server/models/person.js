var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personSchema = new Schema({
    firstName: String,
    lastName: String
});

mongoose.model('persons', personSchema);

