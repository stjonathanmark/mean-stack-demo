var BearerStrategy = require('passport-http-bearer').Strategy;
var jwt = require('jsonwebtoken');

module.exports = {
    configure: (passport) => {
        passport.use(new BearerStrategy(function(token, callback) {
            if (!token) {
                return callback(null, false);
            }
            
            jwt.verify(token, 'InTheNameofJesus', function(err, decoded) {
                if (err) {
                    if (err.name == 'TokenExpiredError') {
                        err.message = 'Your token has expired: ' + error.message;
                    } else {
                        err.message = 'Your token is invalid: ' + error.message;
                    }
                    return callback(err);
                } 
                
                return callback(null, decoded);
            });
        }));
    }
}