var mongoose = require('mongoose');
var objId = require('mongodb').ObjectId;

mongoose.connect('mongodb://localhost/apidata');

var dataSource = {

    getUserByUsername: function(username, callback) {
        mongoose.model('users').findOne({username: username}, function(err, data) {
            callback(err, data);
        });
    },

    getAllPersons: function(callback, skip, limit) {
        skip = skip || 0;
        mongoose.model('persons').count().exec(function(err, count) {
            limit = limit || count;

            mongoose.model('persons')
            .find({})
            .skip(skip)
            .limit(limit)
            .exec(function(err, data) { 
                callback(err, data);
            });
        });
    },

    getPersonById: function(id, callback) {
        mongoose.model('persons').findById(objId.createFromHexString(id), function(err, data) {
            callback(err, data);
        });
    },

    createPerson: function(person, callback) {
        mongoose.model('persons').insertMany(person, function(err, data) {
            callback(err, data);
        });
    },

    updatePerson: function(person, callback) {
        mongoose.model('persons').update({ _id: objId.createFromHexString(person._id) }, person, function(err, data) {
            callback(err, data);
        });
    },

    deletePerson: function(id, callback) {
        mongoose.model('persons').remove({ _id: objId.createFromHexString(id) }, function(err, data) {
            callback(err, data);
        });
    }

}

module.exports = dataSource;