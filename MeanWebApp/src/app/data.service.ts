import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) { }

  getAllPersons() {
    return this.http.get('/api/secure/persons');
  }

  getPerson(id) {
    return this.http.get(`/api/secure/persons/${id}`);
  }

  createPerson(person) {
    return this.http.post('/api/secure/persons', { person: person });
  }

  updatePerson(person) {
    return this.http.put('/api/secure/persons/', { person: person });
  }

  deletePerson(id) {
    return this.http.delete(`/api/secure/persons/${id}`);
  }
}
