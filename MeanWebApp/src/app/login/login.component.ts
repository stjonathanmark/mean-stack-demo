import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SecurityService } from './../auth/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  returnUrl: string = '';
  errorMessage: string = '';

  constructor(private router: Router, private route: ActivatedRoute, private securityService: SecurityService) { }

  login(form: NgForm) {
    this.errorMessage = '';

    let credentials = form.value;

    this.securityService.login(credentials).subscribe(response => {
      if (response.status == 200) {
        let resObj = response.json();

        if (resObj.authenticated) {
          this.securityService.saveToken(resObj.token);
          this.router.navigate([this.returnUrl]);
        } else {
          this.errorMessage = 'Invalid username and/or password. Please try again.';
        }
      } else {
        this.errorMessage = 'An unknown error occurred logging in user.';
      }
    })
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(query => {
      this.returnUrl = query.get('returnUrl') || '/home';
    });
  }

}
