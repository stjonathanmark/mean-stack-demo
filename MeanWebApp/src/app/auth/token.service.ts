import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class TokenService {

    constructor() { }

    tokenExpired(): boolean {
        let token = this.get();
        let expired = true;

        if (token)
        {
            let helper = this.getHelper();
            expired = helper.isTokenExpired(token);
        }

        return expired;
    }

    decode(): any {
        let token = this.get();
        let tokenObj: any = null;

        if (token) {
            let helper = this.getHelper();
            tokenObj = helper.decodeToken(token);
        }

        return tokenObj;
    }

    decodeUrlBase64(): any {
        let token = this.get();
        let tokenObj: any = null;

        if (token) {
            let helper = this.getHelper();
            tokenObj = helper.urlBase64Decode(token);
        }

        return tokenObj;
    }

    save(token: string) {
        sessionStorage.setItem('token', token);
    }

    remove() {
        let token = this.get();

        if (token) {
            sessionStorage.removeItem('token');
        }
    }

    get(): string {
        let token = sessionStorage.getItem('token') || '';

        return token;
    }

    private getHelper(): JwtHelperService {
        let helper = new JwtHelperService();

        return helper;
    }
}



