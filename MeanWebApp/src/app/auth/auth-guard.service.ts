import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';

import { SecurityService } from './security.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private router: Router, private securityService: SecurityService) { }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.securityService.isAuthenticated) return true;

        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

        return false;
    }


    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.securityService.isAuthenticated) return true;

        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

        return false;
    }
}
