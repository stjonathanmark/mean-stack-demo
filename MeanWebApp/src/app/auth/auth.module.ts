import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { TokenService } from './token.service';
import { SecurityService } from './security.service';
import { AuthGuard } from './auth-guard.service';
import { AdminAuthGuard } from './admin-auth-guard.service';

@NgModule({
  imports: [
    RouterModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: { 
        tokenGetter: new TokenService().get, 
        whitelistedDomains:['localhost:3000'],
        blacklistedRoutes: ['localhost:3000/login', 'localhost:3000/api/security/authenticate']
      }
    })
  ],
  providers: [
    TokenService,
    SecurityService,
    AuthGuard,
    AdminAuthGuard
  ]
})
export class AuthModule {}