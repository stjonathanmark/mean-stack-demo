import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { TokenService } from './token.service';

@Injectable()
export class SecurityService {
    constructor(private http: Http, private tokenService: TokenService) { }

    login(credentials: any) {
        return this.http.post('/api/security/authenticate', credentials);
    }

    logout() {
        this.tokenService.remove();
    }

    saveToken(token: string) {
        this.tokenService.save(token);
    }

    get isAuthenticated(): boolean {
        let authenticated = !this.tokenService.tokenExpired();

        return authenticated;
    }

    get user():any {
        return this.tokenService.decode();
    }
}
