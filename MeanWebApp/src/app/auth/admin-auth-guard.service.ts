import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SecurityService } from './security.service';

@Injectable()
export class AdminAuthGuard implements CanActivate, CanActivateChild {

  constructor(private router: Router, private securityService: SecurityService) { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.securityService.user.rle == 'Administrator') return true;

      this.router.navigate(['/not-authorized']);

      return false;
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.securityService.user.rle == 'Administrator') return true;

      this.router.navigate(['/not-authorized']);

      return false;
  }

}
