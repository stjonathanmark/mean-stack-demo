import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { SecurityService } from './auth/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  people: Array<any> = [];

  constructor(private router: Router, private http: HttpClient, private securityService: SecurityService) {

  }

  logout() {
    this.securityService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.http.get('/api/persons').subscribe((response) => { 
       this.people = response as Array<any>; 
    }, (err: HttpErrorResponse) => {

    });
  }

  get loggedIn(): boolean {
    return this.securityService.isAuthenticated;
  }

  get userIsAdmin(): boolean {
    return this.securityService.user.rle == 'Administrator';
  }

}
