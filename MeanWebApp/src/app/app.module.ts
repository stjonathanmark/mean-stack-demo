import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CreatePersonComponent } from './create-person/create-person.component';
import { UpdatePersonComponent } from './update-person/update-person.component';
import { LoginComponent } from './login/login.component';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';

import { DataService } from './data.service';
import { AuthGuard } from './auth/auth-guard.service';
import { AdminAuthGuard } from './auth/admin-auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreatePersonComponent,
    UpdatePersonComponent,
    LoginComponent, 
    NotAuthorizedComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'create-person', component: CreatePersonComponent, canActivate: [AuthGuard, AdminAuthGuard] },
      { path: 'update-person/:id', component: UpdatePersonComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'not-authorized', component: NotAuthorizedComponent },
      { path: '**', redirectTo: 'home' }
    ]),
    AuthModule
  ],
  providers: [DataService, AuthGuard, AdminAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
