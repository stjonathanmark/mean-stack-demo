import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http'
import { DataService} from './../data.service';

@Component({
  selector: 'app-update-person',
  templateUrl: './update-person.component.html',
  styleUrls: ['./update-person.component.css']
})
export class UpdatePersonComponent implements OnInit {

  errorMessage: string = '';
  person: any = { _id: '', firstName: '', lastName: '' };

  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router) { }

  updatePerson(form: NgForm) {
    this.errorMessage = '';

    this.dataService.updatePerson(this.person).subscribe((response) => {
      this.router.navigate(['home']);
    }, (err: HttpErrorResponse) => {
      this.errorMessage = "An unknown error occurred updating person.";
    });
  }

  cancelUpdate() {
    this.router.navigate(['home']);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(map => {
      let id = map.get('id');

      this.dataService.getPerson(id).subscribe((response) => {
        this.person = response;
      }, (err: HttpErrorResponse) => {
        this.errorMessage = 'An unknown error occurred getting person to update.';
      });
    });
  }

}
