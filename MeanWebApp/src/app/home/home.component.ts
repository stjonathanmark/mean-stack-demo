import { Component, OnInit } from '@angular/core';
import { DataService } from './../data.service';
import { SecurityService } from './../auth/security.service';
import { HttpErrorResponse } from '@angular/common/http'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  errorMessage: string = '';
  people: Array<any> = [];

  constructor(private dataService: DataService, private securityService: SecurityService) { }

  deletePerson(id) {
    this.errorMessage = '';
    this.dataService.deletePerson(id).subscribe((response) => {
        this.getAllPersons();
      }, (response: HttpErrorResponse) => {
        this.errorMessage = response.error.message;
      });
  }

  get userIsAdmin(): boolean {
    return this.securityService.user.rle == 'Administrator';
  }

  ngOnInit() {
    this.getAllPersons();
  }

  private getAllPersons() {
    this.errorMessage = '';
    this.dataService.getAllPersons().subscribe((response) => {
      this.people = response as Array<any>;
      },(err: HttpErrorResponse) => {
        this.errorMessage = 'An unknown error occurred getting all persons.';
      });
  }
}
