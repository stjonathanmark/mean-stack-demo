import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { DataService } from "./../data.service";

@Component({
  selector: "app-create-person",
  templateUrl: "./create-person.component.html",
  styleUrls: ["./create-person.component.css"]
})
export class CreatePersonComponent implements OnInit {
  errorMessage: string = "";

  constructor(private dataService: DataService, private router: Router) {}

  createPerson(form: NgForm) {
    this.errorMessage = "";
    let person = form.value;

    this.dataService.createPerson(person).subscribe(
      response => {
        this.router.navigate(["home"]);
      },
      (response: HttpErrorResponse) => {
        this.errorMessage = response.error.message;
      }
    );
  }

  ngOnInit() {}
}
